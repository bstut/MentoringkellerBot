# Dependencies:
#   pip3 install bs4
#   pip3 install python-telegram-bot

import logging
import sys
import telegram
from telegram.ext import Updater, CommandHandler
from bs4 import BeautifulSoup as bs
from urllib.request import urlopen


LICENSE_TEXT = 'Welcome!\nThis bot is a program which is available under the MIT license at https://gitlab.com/Bergiu/MentoringkellerBot'

APS = ["ap-2356-6u07", "ap-2356-6u09"]
URL = "https://noc-portal.rz.rwth-aachen.de/mops-admin/coverage?iib=173"


def get_mentoringkeller_aps():
    soup = bs(urlopen(URL), features="html5lib")
    out = []
    for ap in soup.findAll("tr"):
        name_ = ap.td
        if name_ is None:
            continue
        name = name_.string
        if name in APS:
            out.append(ap)
    return out


def get_user_count(maps):
    counts = []
    for map_ in maps:
        # each ap
        ap_count = 0
        children = map_.children
        i = 0
        for child in children:
            if i == 1:
                # name
                name = child.contents[0]
            if i == 3:
                # 2.5ghz
                count = int(child.contents[0])
                ap_count += count
            if i == 5:
                # 5ghz
                count = int(child.contents[0])
                ap_count += count
            i += 1
        ap = {"name": name, "count": ap_count}
        counts.append(ap)
    return counts


def count(bot, update):
    maps = get_mentoringkeller_aps()
    counts = get_user_count(maps)
    text = "Mentoringkeller Geräte:\n\n"
    text += "<code>"
    for ap in counts:
        text += ap["name"] + ": " + str(ap["count"])+"\n"
    text += "</code>"
    bot.send_message(chat_id=update.message.chat_id, text=text, parse_mode=telegram.ParseMode.HTML)


def license(bot, update):
    bot.send_message(chat_id=update.message.chat_id, text=LICENSE_TEXT, parse_mode=telegram.ParseMode.HTML)


def main():
    if len(sys.argv) < 1:
        print("ERROR: first parameter must be the telegram token")
        sys.exit(1)
    token = sys.argv[1]
    updater = Updater(token=token)
    dispatcher = updater.dispatcher
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
    count_handler = CommandHandler('count', count)
    dispatcher.add_handler(count_handler)
    license_handler = CommandHandler('license', license)
    dispatcher.add_handler(license_handler)
    updater.start_polling()


main()
